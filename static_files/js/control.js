$("document").ready(function () {

    let checkTcNum = function (value) {
        value = value.toString();
        let isEleven = /^[0-9]{11}$/.test(value);
        let totalX = 0;
        for (let i = 0; i < 10; i++) {
            totalX += Number(value.substr(i, 1));
        }
        let isRuleX = totalX % 10 == value.substr(10, 1);
        let totalY1 = 0;
        let totalY2 = 0;
        for (let i = 0; i < 10; i += 2) {
            totalY1 += Number(value.substr(i, 1));
        }
        for (let i = 1; i < 10; i += 2) {
            totalY2 += Number(value.substr(i, 1));
        }
        let isRuleY = ((totalY1 * 7) - totalY2) % 10 == value.substr(9, 0);
        return isEleven && isRuleX && isRuleY;
    };
    const checkedControl = $('.tc-control');
    const checkedImage = $('.checked_img');
    const closeImage = $('.close_img');
    const inputTc = $('input#tc');
    const inputUsername = $('input#username');
    const inputPass = $('input#password');
    const inputPhone = $('input#phone');
    const inputCarBrand = $('input#car_brand');
    const inputNumberPlate = $('input#number_plate');
    const inputChasisNumber = $('input#chasis_number');
    inputTc.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = checkTcNum($(this).val());
        if (isValid) {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    inputUsername.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    inputPass.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    $('#step-one input').on('keyup focus blur load', function () {
        if (inputTc.val() !== '' && inputUsername.val() !== '' && inputPass.val() !== '') {
            $('#step-one-btn').removeAttr('disabled');
        } else {
            $('#step-one-btn').attr('disabled', 'false');
        }
    });
    //1 Finish


    inputPhone.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    inputCarBrand.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    $('#step-two input').on('keyup focus blur load', function () {
        if (inputPhone.val() !== '' && inputCarBrand.val() !== '') {
            $('#step-two-btn').removeAttr('disabled');
        } else {
            $('#step-two-btn').attr('disabled', 'false');
        }
    });
    //2 Finish


    inputNumberPlate.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    inputChasisNumber.on('keyup focus blur load', function (event) {
        event.preventDefault();
        const isValid = $(this).val();
        if (isValid !== '') {
            $(this).next(checkedControl).find(checkedImage).show();
            $(this).next(checkedControl).find(closeImage).hide();
        } else {
            $(this).next(checkedControl).find(closeImage).show();
            $(this).next(checkedControl).find(checkedImage).hide();
        }
    });
    $('#step-three input').on('keyup focus blur load', function () {
        if (inputNumberPlate.val() !== '' && inputChasisNumber.val() !== '') {
            $('#step-three-btn').removeAttr('disabled');
        } else {
            $('#step-three-btn').attr('disabled', 'false');
        }
    });
    //3 Finish
}); //document.ready