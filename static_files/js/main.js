$(document).ready(function () {
    const NextButton = $('button');
    const imageListing = $('#image-listing');
    const imageListingClose = $('.close_listing');
    const inputImage = $('input#image');
    const inputDefaultImage = $('input#image_default');
    const bgImg = $('label[for=image_default] .img_bg');
    const keyOne = $('#key_one');
    const keyTwo = $('#key_two');
    const Success = $('#success');
    $('input[name=tc], input[name=phone]').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('label[for=image_default]').on('click', function () {
        imageListing.addClass('active');
    });
    imageListingClose.on('click', function () {
        imageListing.removeClass('active');
    });

    imageListing.find('li').on('click', function () {
        const order = $(this).data('order');
        const title = $(this).data('title');
        const img = $(this).data('img');
        inputImage.val(order);
        inputDefaultImage.val(title);
        bgImg.css('background-image', 'url(' + img + ')');
        imageListing.removeClass('active');
    });
    NextButton.on('click', function (event) {
        event.preventDefault();
        let form = $(this).parent('form');
        let formData;
        if (event.target.id === 'step-one-btn') {
            formData = form.serializeArray();
            $.ajax({
                url: '/member/step1',
                method: 'POST',
                data: formData,
                success: function (res) {
                    if (res.status === 'success') {
                        form.removeClass('active');
                        form.next('form').addClass('active');
                        keyOne.val(res.anahtar);
                    }
                }, error: function (res) {
                    console.log(res)
                }
            });
        }
        if (event.target.id === 'step-two-btn') {
            formData = form.serializeArray();
            $.ajax({
                url: '/member/step2',
                method: 'POST',
                data: formData,
                success: function (res) {
                    if (res.status === 'success') {
                        form.removeClass('active');
                        form.next('form').addClass('active');
                        keyTwo.val(res.anahtar);
                    }
                }, error: function (res) {
                    console.log('Error');
                }
            });
        }
        if (event.target.id === 'step-three-btn') {
            formData = form.serializeArray();
            $.ajax({
                url: '/member/step3',
                method: 'POST',
                data: formData,
                success: function (res) {
                    if (res.status === 'success') {
                        form.removeClass('active');
                        Success.addClass('active');
                    }
                }, error: function (res) {
                    console.log('Error');
                }
            });
        }
    });
});