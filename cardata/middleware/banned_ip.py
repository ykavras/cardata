from django import http
from cardata.apps.bannedip.models import BannedIp


class BlockedIpMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.META.get('HTTP_X_REAL_IP') in BannedIp.objects.values_list('ip', flat=True):
            return http.HttpResponseForbidden('<h1 style="text-align:center">Forbidden</h1>')
        return response
