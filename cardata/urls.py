from django.contrib import admin
from django.urls import path, include
from cardata.apps.members import urls as member_urls
from cardata.apps.home import urls as home_url

from cardata import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('member/', include(member_urls, namespace='member')),
    path('', include(home_url, namespace='home')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
