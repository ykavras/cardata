from django.core.exceptions import ValidationError
import re


def validate_tckn(value):
    error_text = "TC Kimlik numarası hatalı"
    if not (len(value) == 11 and value.isdigit() and value[0] != '0'):
        raise ValidationError(error_text)

    digits = [int(d) for d in str(value)]

    if not sum(digits[:10]) % 10 == digits[10]:
        raise ValidationError(error_text)

    if not (((7 * sum(digits[:9][-1::-2])) - sum(digits[:9][-2::-2])) % 10) == digits[9]:
        raise ValidationError(error_text)
    return True


def validate_username(value):
    if not re.fullmatch(r'^[A-Za-z0-9]{5,13}$', value).group() == value:
        raise ValidationError(
            'Kullanıcı adı hatalı! Kullanıcı adınız: Sadece harf(İngizlice) ve sayılardan, en az 6 ve en fazla 12 karakter uzunluğunda olmalı')


def validate_phone(value):
    if len(value) != 10 and not value.isdigit():
        raise ValidationError('Cep telefonu hatalı! Cep telefonu: 10 haneli rakamlardan oluşmalıdır.')
