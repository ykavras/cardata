from django.shortcuts import render
from cardata.apps.members.models import Picture


def home(request):
    pictures = Picture.objects.all()
    payload = {
        'bodyId': 'home-page',
        'title': 'Anasayfa',
        'pictures': pictures
    }
    return render(request, 'home.html', payload)
