from django.db import models


class BannedIp(models.Model):
    ip = models.CharField(max_length=20)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = 'Banlı IP'
        verbose_name_plural = 'Banlanan IPler'
