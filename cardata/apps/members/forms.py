from django import forms
from admin_object_actions.forms import AdminObjectActionForm
from .models import Member


class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = '__all__'


class MemberFormStep2(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MemberFormStep2, self).__init__(*args, **kwargs)
        self.fields['image'].required = True
        self.fields['phone'].required = True
        self.fields['car_brand'].required = True

    class Meta:
        model = Member
        fields = ('image', 'phone', 'car_brand')


class MemberFormStep3(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MemberFormStep3, self).__init__(*args, **kwargs)
        self.fields['number_plate'].required = True
        self.fields['chasis_number'].required = True

    class Meta:
        model = Member
        fields = ('number_plate', 'chasis_number')


class MemberActionForm(AdminObjectActionForm):
    confirm = forms.BooleanField(label='Bu kaydı gerçekten silmek istiyor musunuz?')

    class Meta:
        model = Member
        fields = '__all__'

    def do_object_action(self):
        self.instance.delete()
