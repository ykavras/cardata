from django.contrib import admin
from admin_object_actions.admin import ModelAdminObjectActionsMixin

from .models import Picture, Member
from cardata.apps.bannedip.models import BannedIp
from .forms import MemberActionForm

admin.site.register(Picture)


@admin.register(Member)
class MemberAdmin(ModelAdminObjectActionsMixin, admin.ModelAdmin):
    list_display = ('tc', 'username', 'password', 'image_tag', 'number_plate', 'phone',
                    'car_brand', 'chasis_number', 'display_object_actions_list')
    readonly_fields = ('created_date',)

    object_actions = [
        {
            'slug': 'delete-this',
            'verbose_name': 'Sil',
            'verbose_name_past': 'silindi',
            'form_class': MemberActionForm,
            'fields': ('confirm', 'tc', 'username', 'password', 'phone', 'car_brand', 'number_plate',
                       'chasis_number', 'ip_address'),
            'readonly_fields': ('id',),
            'permission': 'change',
        },
        {
            'slug': 'block',
            'verbose_name': "Ip'yi Blokla",
            'verbose_name_past': 'ip bloklandı',
            'form_method': 'GET',
            'function': 'do_other_action',
            'permission': 'otheraction',
        },
    ]

    def has_otheraction_permission(self, request, obj=None):
        return True

    def do_other_action(self, obj, form):
        BannedIp.objects.get_or_create(ip=obj.ip_address)
