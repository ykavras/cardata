from django.db import models
from uuid import uuid4
from django.utils.html import mark_safe

from cardata.utils.validations import validate_tckn, validate_username, validate_phone


class Picture(models.Model):
    display_text = models.CharField(verbose_name='Resimin ismi', max_length=90)
    image = models.ImageField(verbose_name='Resim', upload_to='resimler/')
    order = models.SmallIntegerField(verbose_name='Sırası', null=True, blank=True)

    def __str__(self):
        return self.display_text

    class Meta:
        verbose_name = 'Resim'
        verbose_name_plural = 'Resimler'
        ordering = ['order', ]


class Member(models.Model):
    created_date = models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi')
    uuid = models.UUIDField(unique=True, default=uuid4, editable=False, null=True, blank=True)
    tc = models.CharField(verbose_name='TCKN', validators=[validate_tckn], max_length=11)
    username = models.CharField(verbose_name='Kullanici Adı', validators=[validate_username], max_length=12)
    password = models.CharField(verbose_name='Parola', max_length=6)
    image = models.ForeignKey(Picture, verbose_name='Resim', related_name='member', on_delete=models.CASCADE, null=True, blank=True)
    phone = models.CharField(verbose_name='Cep Telefonu', max_length=10, validators=[validate_phone], null=True, blank=True)
    car_brand = models.CharField(verbose_name='Araç Markası', max_length=20, null=True, blank=True)
    number_plate = models.CharField(verbose_name='Plaka No', max_length=15, null=True, blank=True)
    chasis_number = models.CharField(verbose_name='Şase No', max_length=25, null=True, blank=True)
    ip_address = models.CharField(verbose_name='IP Adresi', max_length=20, null=True, blank=True)

    def __str__(self):
        return self.tc

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="{}" width="40" height="20"/><span> - {}</span>'.format(self.image.image.url, self.image.display_text))
        return '------'

    image_tag.short_description = 'Resim'

    class Meta:
        verbose_name = 'Üye'
        verbose_name_plural = 'Üyeler'
        ordering = ['-id']
