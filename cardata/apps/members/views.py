from django.shortcuts import render
from django.http import JsonResponse
from uuid import uuid4
from .forms import MemberForm, MemberFormStep2, MemberFormStep3
from .models import Member


def step1(request):
    if request.method == 'POST':
        form = MemberForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.ip_address = request.META.get('HTTP_X_REAL_IP')
            instance.save()
            return JsonResponse({'status': 'success', 'anahtar': instance.uuid})
        else:
            return JsonResponse({'status': 'warning', 'message': form.errors})
    else:
        return JsonResponse({'status': 'error', 'message': 'Hatalı istek!'})


def step2(request):
    if request.method == 'POST':
        uuid = request.POST.get('anahtar')
        obj = Member.objects.get(uuid=uuid)
        if obj:
            form = MemberFormStep2(request.POST or None, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.uuid = uuid4()
                instance.save()
                return JsonResponse({'status': 'success', 'anahtar': instance.uuid})
            else:
                return JsonResponse({'status': 'warning', 'message': form.errors})
    else:
        return JsonResponse({'status': 'error', 'message': 'Hatalı istek!'})


def step3(request):
    if request.method == 'POST':
        uuid = request.POST.get('anahtar')
        obj = Member.objects.get(uuid=uuid)
        if obj:
            form = MemberFormStep3(request.POST, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.uuid = uuid4()
                instance.save()
                return JsonResponse({'status': 'success'})
            else:
                return JsonResponse({'status': 'warning', 'message': form.errors})
    else:
        return JsonResponse({'status': 'error', 'message': 'Hatalı istek!'})