from django.urls import path

from .views import step1, step2, step3

app_name = 'members'

urlpatterns = [
    path('step1', step1, name='step1'),
    path('step2', step2, name='step2'),
    path('step3', step3, name='step3'),
]
